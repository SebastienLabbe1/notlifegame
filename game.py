#!/usr/bin/env python3
import pygame, random, numpy, neat, time
from itertools import product
from brains import *

#GLOBAL VARIABLES
gen = 0
tick = 0

MAXGEN = 10000
MAXTICKS = 1000

INITIALDAILYDAMAGE = 0
GROWTHDAILYDAMAGE = 0

INITIALTICKREWARD = 0.1
GROWTHTICKREWARD = 0

WORLDSIZE = 60
WINDOWSIZE = 600

ENEMYSPAWNRATE = 0.1

def randomColor():
	return numpy.array([random.randint(0, 255),random.randint(0, 255), random.randint(0, 255)])

class Genome:
	fitness = 0
	
class Creature:
	"""
	class of creature that will roam around in the world

	possible actions:
	move 0
	turn 1, 2
	attack 3
	eat 4
	jump 5
	"""

	directions = [numpy.array(x) for x in [[0, 1], [1, 0], [0, -1], [-1, 0]]]

	viewDist = 10

	maxLife = 100

	attackCost = 0
	attackDamage = 100


	def __init__(self, pos, brain, genome = Genome()):
		self.pos = pos
		self.brain = brain
		self.genome = genome

		self.life = self.maxLife

		self.color = randomColor()
		self.dir = random.randint(0, 3)

	def getColor(self):
		return self.color
	
	def getAction(self, info):
		return self.brain.activate(info)
	
	def getPos(self):
		return self.pos

	def setPos(self, pos):
		self.pos = pos
	
	def getDir(self):
		return Creature.directions[self.dir]
	
	def turn(self, i):
		self.dir = (self.dir + i) % 4
	
	def changeLife(self, change):
		if self.isAlive():
			self.life = min(self.life + change, Creature.maxLife)
	
	def isAlive(self):
		return self.life > 0
	
	def getLifeP(self):
		return self.life / self.maxLife

	def reward(self, x):
		self.genome.fitness += x
	
class Cell:
	color = numpy.array([0, 0, 0])
	def __init__(self):
		return

	def getColor(self):
		return self.color
	
	
class FoodCell(Cell):
	maxFood = 1000
	foodGrowthRate = 0.1
	eatingCost = 0.01
	initialFood = 100
	color = numpy.array([0, 255, 0])
	def __init__(self):
		Cell.__init__(self)
		self.lastFood = self.initialFood
		self.lastTick = tick

	def updateFood(self):
		self.lastFood = min(self.lastFood + self.foodGrowthRate * (tick - self.lastTick), self.maxFood)
		self.lastTick = tick

	def getFood(self):
		self.updateFood()
		return self.lastFood

	def getFoodP(self):
		return self.getFood() / self.maxFood
	
	def eatFood(self):
		food = self.getFood()
		self.lastFood = 0
		self.foodGrowth = max(0, self.foodGrowthRate - self.eatingCost)
		return food

	
	def getColor(self):
		self.updateFood()
		r = self.lastFood / self.maxFood
		return self.color * r

class World:
	def __init__(self, n, visualize = False):
		self.n = n
		self.cellMap = [FoodCell() for i in range(n * n)]
		self.creatureMap = [-1 for i in range(n * n)]
		self.creatures = []
		self.visualize = visualize
		self.totalKills = 0
		self.running = False
	
	def getI(self, pos):
		if not 0 <= pos[0] < self.n or not 0 <= pos[1] < self.n:
			return -1
		return int(pos[0]) * self.n + int(pos[1])
	
	def getPos(self, i):
		if not 0 <= i < self.n * self.n:
			return -1
		return numpy.array([i // self.n, i % self.n])
	
	def createPatrols(self):
		r = ENEMYSPAWNRATE
		while random.random() < r:
			r -= 1
			pos = self.getFreePosition()
			if pos[0] != -1:
				creature = Creature(pos, PatrolBrain())
				creature.color = numpy.array([255, 0, 0])
				self.addCreature(creature)
			else:
				break

	def getIDCount(self, IDs):
		count = 0
		for creature in self.creatures:
			if creature.brain.ID in IDs:
				count += 1
		return count



	def nextTick(self):
		global tick
		tick += 1
		nC = len(self.creatures)
		for i in range(nC):
			self.creatureAction(i)
		for i in range(nC-1, -1, -1):
			if not self.creatures[i].isAlive():
				self.removeCreature(i)
		tickReward = INITIALTICKREWARD + GROWTHTICKREWARD * tick
		for creature in self.creatures:
			creature.reward(tickReward)
		if self.getIDCount([1, 0]) == 0 or tick == MAXTICKS:
			self.running = False
			return
		self.createPatrols()

	def nearestCreature(self, x, dx, l):
		for i in range(l):
			iI = self.getI(x + dx * i)
			if iI == -1:
				return (-1, l)
			if self.creatureMap[iI] != -1:
				return (self.creatureMap[iI], i)
		return (-1, l)

	def getFoodP(self, pos):
		posI = self.getI(pos)
		if posI == -1: return 0
		return self.cellMap[posI].getFoodP()

	def getInfo(self, iC):	
		# info = (life, foodincell, dist to enemies in 3 eyes, food in 3 adjacent cells)
		pos = self.creatures[iC].getPos()
		dire = self.creatures[iC].getDir()
		posI = self.getI(pos)
		
		viewDist = self.creatures[iC].viewDist

		self.creatures[iC].turn(1)

		pos0 = pos + self.creatures[iC].getDir()
		dist0 = 1 - self.nearestCreature(pos0, dire, viewDist)[1]/viewDist
		food0 = self.getFoodP(pos0)

		self.creatures[iC].turn(-1)

		pos1 = pos + self.creatures[iC].getDir()
		dist1 = 1 - self.nearestCreature(pos1, dire, viewDist)[1]/viewDist
		food1 = self.getFoodP(pos1)

		self.creatures[iC].turn(-1)

		pos2 = pos + self.creatures[iC].getDir()
		dist2 = 1 - self.nearestCreature(pos2, dire, viewDist)[1]/viewDist
		food2 = self.getFoodP(pos2)

		self.creatures[iC].turn(1)


		life = self.creatures[iC].getLifeP()
		food = self.cellMap[posI].getFoodP()
		#print(life, food, dist0, dist1, dist2)
		return (life, food, dist0, dist1, dist2)#, food0, food1, food2)
	
	def getFreePosition(self):
		n2 = self.n * self.n
		end = random.randint(0, n2 - 1)
		i = (end + 1) % n2
		while i != end and self.creatureMap[i] != -1:
			i = (i + 1) % n2
		if i == end:
			return [-1, -1]
		return self.getPos(i)
	
	def moveCreature(self, iC, distance):
		creature = self.creatures[iC]
		pos = creature.getPos()
		dire = creature.getDir()
		newPos = pos + dire
		newPosI = self.getI(newPos)
		if newPosI != -1 and self.creatureMap[newPosI] == -1:
			self.creatureMap[self.getI(pos)] = -1
			self.creatureMap[newPosI] = iC
			creature.setPos(newPos)
	
	def shoot(self, iC):
		creature = self.creatures[iC]
		pos = creature.getPos()
		dire = creature.getDir()
		posI = self.getI(pos)
		viewDist = creature.viewDist
		attackIC = self.nearestCreature(pos + dire, dire, viewDist)[0]
		if attackIC != -1 and self.creatures[attackIC].isAlive():
			attackPos = self.creatures[attackIC].getPos()
			attackI = self.getI(attackPos)
			self.creatures[attackIC].changeLife(-creature.attackDamage)
			if not self.creatures[attackIC].isAlive():
				self.totalKills += 1

	def creatureAction(self, iC):
		creature = self.creatures[iC]
		iA = creature.getAction(self.getInfo(iC))
		pos = creature.getPos()
		dire = creature.getDir()
		posI = self.getI(pos)
		lifeChange = -(INITIALDAILYDAMAGE + GROWTHDAILYDAMAGE * tick)
		if iA == 0:
			self.moveCreature(iC, 1)
		elif iA == 1:
			creature.turn(1)
		elif iA == 2:
			creature.turn(-1)
		elif iA == 3:
			self.shoot(iC)
		elif iA == 4:
			lifeChange += self.cellMap[posI].eatFood()
		elif iA == 5:
			self.moveCreature(iC, 2)
			
		creature.changeLife(lifeChange)

	def addCreature(self, creature):
		pos = creature.getPos()
		posI = self.getI(pos)
		self.creatures.append(creature)
		self.creatureMap[posI] = len(self.creatures) - 1

	def removeCreature(self, iC):
		if not 0 <= iC < len(self.creatures):
			return
		if iC != len(self.creatures) - 1:
			pos2 = self.creatures[-1].getPos()
			i2 = self.getI(pos2)
			self.creatureMap[i2] = iC
			self.creatures[-1], self.creatures[iC] = self.creatures[iC], self.creatures[-1]
		pos1 = self.creatures[-1].getPos()
		i1 = self.getI(pos1)
		self.creatureMap[i1] = -1
		self.creatures.pop(-1)
	
	def gameLoop(self):
		self.running = True
		if not self.visualize:
			while self.running:
				print("tick", tick, end="\r")
				self.nextTick()
		else:
			cellSize = int(WINDOWSIZE / self.n)
			pygame.init()
			L = cellSize * self.n
			screen = pygame.display.set_mode([L, L])

			while self.running:
				print("tick", tick, end="\r")
				
				for i, j in product(range(self.n), range(self.n)):
					pos = numpy.array([i, j])
					iI = self.getI(pos)
					rect = (cellSize * i, cellSize * j, cellSize, cellSize)
					if self.creatureMap[iI] != -1:
						color = self.creatures[self.creatureMap[iI]].getColor()
						pygame.draw.rect(screen, color, rect)
						dire = self.creatures[self.creatureMap[iI]].getDir()
						center = [int(x * cellSize) for x in (pos + 0.3 * dire + [0.5, 0.5])]
						pygame.draw.circle(screen, (255, 255, 255), center, int(0.2 * cellSize))
					else:
						color = self.cellMap[iI].getColor()
						pygame.draw.rect(screen, color, rect)

				pygame.display.flip()

				self.nextTick()

				for event in pygame.event.get():
					if event.type == pygame.QUIT:
						self.running = False
			pygame.quit()

		print("Kills :", self.totalKills)

def playGame():
	global tick
	tick = 0
	world = World(10, True)
	creature = Creature(world.getFreePosition(), HumanBrain())
	world.addCreature(creature)
	for i in range(2):
		creature = Creature(world.getFreePosition(), DeadBrain())
		world.addCreature(creature)
	world.gameLoop()
	
	
def evalGeneration(genomes, config):
	global tick, gen
	tick = 0
	gen += 1
	visualize = (gen % 3) == 0
	if len(genomes) > WORLDSIZE * WORLDSIZE:
		exit("too many creatures for this world")
	world = World(WORLDSIZE, visualize)
	for genome_id, genome in genomes:
		genome.fitness = 0
		brain = NNetBrain(neat.nn.FeedForwardNetwork.create(genome, config))
		creature = Creature(world.getFreePosition(), brain, genome)
		world.addCreature(creature)
	world.gameLoop()

def tryNeat(config_filename):
	#g = neat.ActivationFunctionSet()
	#print(g)
	config = neat.config.Config(neat.DefaultGenome, neat.DefaultReproduction,\
		neat.DefaultSpeciesSet, neat.DefaultStagnation, config_filename)

	p = neat.Population(config)
	p.add_reporter(neat.StdOutReporter(True))
	stats = neat.StatisticsReporter()
	p.add_reporter(stats)

	winner = p.run(evalGeneration, MAXGEN)
	print('\nBest genome:\n{!s}'.format(winner))

playGame()
#tryNeat("config.txt")

