import numpy as np
import pygame, config


class GUI:
    def __init__(self, world, focusObject):
        self.world = world
        self.focusObject = focusObject

        self.screenCenter = np.zeros(2)
        self.screenRatio = 1.
        self.screenPos = np.zeros(2)
        self.screenDiag = np.zeros(2)

        self.maxFPS = 60
        self.size = np.zeros(2)

    def setScreenSize(self, ratio = 0.7):
        info = pygame.display.Info()
        self.size = (np.array([info.current_w, info.current_h]) * ratio).astype(int)

    def zoom(self, z):
        self.screenRatio *= z

    def screenCoordinates(self, x):
        return np.multiply(np.divide(x - self.screenPos, self.screenDiag), self.size).astype(int)
    
    def screenSize(self, x):
        return int(x * self.screenRatio)
    
    def screenSetup(self):
        speedN = np.linalg.norm(self.focusObject.speed)
        self.screenRatio = 1 if speedN < config.EPS else (speedN + 1) / speedN
        self.screenCenter = self.focusObject.getPos()
        self.screenPos = self.screenCenter - self.size / (2 * self.screenRatio)
        self.screenDiag = self.size / self.screenRatio

    def run(self):
        self.running = True
        clock = pygame.time.Clock()
        pygame.init()

        self.setScreenSize()

        flags = pygame.DOUBLEBUF
        screen = pygame.display.set_mode(self.size, flags)
        screen.fill((255, 255, 255))
        pygame.display.update()

        
        #t0 = time.clock()
        #lastFrame = 0
        #frame = 0
        oldRects = []

        while self.running:
            #if not frame % 100:
                #t1 = time.clock()
                #if self.tick != 0:
                    #print("tick", self.tick, "FPS :", (self.tick - lastTick) / (t1 - t0), end="\r")
                #t0 = t1
                #lastTick = self.tick
            dt = clock.tick(self.maxFPS)
            self.world.nextTick(dt)

            self.screenSetup()

            screen.fill((255, 255, 255))
            
            rects = []
            for rect, color in self.world.getRects():
                srect = [self.screenCoordinates(x) for x in rect]
                rects.append(pygame.draw.polygon(screen, color, srect))
            for center, radius, color in self.world.getCircles():
                scenter = self.screenCoordinates(center)
                sradius = self.screenSize(radius)
                rects.append(pygame.draw.circle(screen, color, scenter, sradius))

            activerects = rects + oldRects
            pygame.display.update(activerects)
            oldRects = rects
        pygame.quit()
