import numpy as np
import pygame, config, time, physics

class World:
    def __init__(self, worldMin, worldMax):
        self.objects = []
        self.dt = 0

        self.worldMin = worldMin
        self.worldMax = worldMax 

        self.friction = config.FRICTION
        self.globalForce = config.GRAVITY

        self.tick = 0
        self.centerSize = 2
    
    def addObject(self, obj, pos = [None]):
        if pos[0] == None:
            pos = self.worldMin + np.random.rand(2) * (self.worldMax - self.worldMin)
        obj.moveTo(pos)
        self.objects.append(obj)
        obj.setWorld(self)

    def nextTick(self, dt):
        self.dt = dt * config.DT
        self.friction = config.FRICTION ** self.dt
        self.tick += 1

        for i,obj1 in enumerate(self.objects):
            for j,obj2 in enumerate(self.objects[:i]):
                if physics.doPhysics(obj1, obj2):
                    obj1.collision(obj2)
                    obj2.collision(obj1)

        for obj in self.objects:
            obj.action([])

        for obj in self.objects:
            obj.updatePos()

        isAlive = lambda obj : obj.isAlive()

        self.objects = list(filter(isAlive, self.objects))

    def getRects(self):
        for obj in self.objects:
            for rect, color in obj.getRects():
                yield (rect, color)

    def getCircles(self):
        for obj in self.objects:
            for center, radius, color in obj.getCircles():
                yield (center, radius, color)
            if not obj.lockAngle:
                yield (obj.getPos(), self.centerSize, (0, 0, 0))
