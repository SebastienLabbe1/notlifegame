import numpy as np

def randomColor():
    return (np.random.rand(3) * 255).astype(int)

def turn90(v):
    return np.array([v[1], -v[0]])

def rotate(points, theta):
    cos, sin = np.cos(theta), np.sin(theta)
    R = np.array(((cos, sin), (-sin, cos)))
    return [p.dot(R) for p in points]
