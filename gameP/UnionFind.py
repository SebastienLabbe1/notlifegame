class UF:
    def __init__(self, n):
        self.n = n
        self.p = [-1 for i in range(n)]
        self.mins = {i:i for i in range(n)}
        self.maxs = {i:i for i in range(n)}
    
    def P(self, i):
        while self.p[i] >= 0:
            if self.p[self.p[i]] >= 0:
                self.p[i] = self.p[self.p[i]]
            i = self.p[i]
        return i

    def U(self, a, b):
        pa = self.P(a)
        pb = self.P(b)
        if pa == pb:
            return
        if(self.p[pa] > self.p[pb]):
            self.mins[pb] = min(self.mins[pb], self.mins[pa])
            self.maxs[pb] = max(self.maxs[pb], self.maxs[pa])
            self.mins.pop(pa)
            self.maxs.pop(pa)
            self.p[pb] += self.p[pa]
            self.p[pa] = pb
        else:
            self.mins[pa] = min(self.mins[pb], self.mins[pa])
            self.maxs[pa] = max(self.maxs[pb], self.maxs[pa])
            self.mins.pop(pb)
            self.maxs.pop(pb)
            self.p[pa] += self.p[pb]
            self.p[pb] = pa
    
    def S(self, i):
        return -self.p[self.P(i)]
