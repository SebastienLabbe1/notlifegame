#!/usr/bin/env python3
import pygame, random, neat, time, math, numpy as np
import config, physics, objects, world, GUI

def playGame():
    worldMin, worldMax = np.zeros(2), np.array([1000, 100])
    w = world.World(worldMin, worldMax)

    player = objects.Player()
    w.addObject(player)

    for i in range(0):
        w.addObject(objects.StupidCreature())
    
    for i in range(0):
        w.addObject(objects.Food(10))
    
    size = np.array([100, 20])
    nR = 4
    w.addObject(objects.Lava(size))
    w.addObject(objects.Rect(size, False))
    w.addObject(objects.Rect(size))

    w.addObject(objects.Circle(10))

    w.addObject(objects.Rect(size))
    #w.addObject(objects.Rect(np.array([w.size[0]/2, 10])), np.array([w.size[0]/2, 0]))

    #w.addObject(objects.Rect(np.array([10, w.size[1]/2])), np.array([0, w.size[1]/2]))
    #w.addObject(objects.Rect(np.array([10, w.size[1]/2])), np.array([w.size[0] - 10, w.size[1]/2]))
    gui = GUI.GUI(w, player)
    gui.run()
    
    
    
def evalGeneration(genomes, config):
    global tick, gen
    tick = 0
    gen += 1
    visualize = (gen % 3) == 0
    if len(genomes) > WORLDSIZE * WORLDSIZE:
        exit("too many creatures for this world")
    world = World(WORLDSIZE, visualize)
    for genome_id, genome in genomes:
        genome.fitness = 0
        brain = NNetBrain(neat.nn.FeedForwardNetwork.create(genome, config))
        creature = Creature(world.getFreePosition(), brain, genome)
        world.addCreature(creature)
    world.gameLoop()

def tryNeat(config_filename):
    #g = neat.ActivationFunctionSet()
    #print(g)
    config = neat.config.Config(neat.DefaultGenome, neat.DefaultReproduction,\
        neat.DefaultSpeciesSet, neat.DefaultStagnation, config_filename)

    p = neat.Population(config)
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)

    winner = p.run(evalGeneration, MAXGEN)
    print('\nBest genome:\n{!s}'.format(winner))

gen = 0
tick = 0

playGame()
#tryNeat("config.txt")
this_is_a_test = "hello"
this_is_a_test

