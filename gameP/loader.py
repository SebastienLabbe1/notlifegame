from scipy import misc
import glob
import imageio
import UnionFind as UF
import numpy as np
import pickle

#for image_path in glob.glob(filename):

class WorldDescription:
    def __init__(self, wmin = [0,0], wmax = [0,0]):
        self.wmin = wmin
        self.wmax = wmax
        self.objects = []

    def addObject(self, a, b, color):
        self.objects.append((a, b, color))

    def load(self, filename):
        wd = pickle.load(filename)
        self.wmin = wd.wmin
        self.wmax = wd.wmax
        self.objects = wd.objects

    def save(self, filename):
        pickle.dump(self, filename)

"""
    def load(self, filename):
        with open(filename) as file:
            line = file.readline()
            self.wmin[0], self.wmin[1], self.wmax[0], self.wmax[1]

    def save(self, filename):
        with open(filename) as file:
            file.write("{} {} {} {}\n".format(self.wmin[0], self.wmin[1], self.wmax[0], self.wmax[1]))
            file.write("{}\n".format(len(self.objects)))
            for a, b, c in self.objects:
                file.write("{} {} {} {} {} {} {} {}\n".format(a[0], a[1], b[0], b[1], c[0], c[1], c[2], c[3]))
            line = file.readline()
"""

def loadWorld(filename, maxDist = 1):
    image = imageio.imread(filename)
    w, h, _ = image.shape
    uf = UF.UF(w*h)
    for j in range(h):
        print(j)
        if j < h - 1:
            for i in range(w):
                a = np.array(image[i][j])
                b = np.array(image[i][j+1])
                if np.linalg.norm(a - b) < maxDist:
                    uf.U(i*h + j, i*h + j+1)
                if i < w - 1:
                    a = np.array(image[i][j])
                    b = np.array(image[i+1][j])
                    if np.linalg.norm(a - b) < maxDist:
                        uf.U(i*h + j, (i+1)*h + j)
        else:
            for i in range(w):
                if i < w - 1:
                    a = np.array(image[i][j])
                    b = np.array(image[i+1][j])
                    if np.linalg.norm(a - b) < maxDist:
                        uf.U(i*h + j, (i+1)*h + j)
    wd = WorldDescription([0, 0], [w, h])
    for i in uf.mins.keys():
        a = (uf.mins[i] // h, uf.mins[i] % h)
        b = (uf.maxs[i] // h, uf.maxs[i] % h)
        c = image[i//h][i%h]
        wd.addObject(a, b, c)
    wd.save(filename[:-3] + "description")

    print(uf.mins)
    print(uf.maxs)
    print(image.shape)
    print(image.dtype)

loadWorld("worlds/world1.png")
