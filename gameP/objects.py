import numpy as np
import math, pygame, config, utils

class Object:
    color = np.array([0, 0, 0])

    #States
    ALIVE = 0
    DEAD = 1
    state = ALIVE
    
    lockPos = False
    lockAngle = False

    onFloor = False

    stiffness = 1

    def __init__(self):
        self.pos = np.zeros(2)
        self.speed = np.zeros(2)
        self.acceleration = np.zeros(2)

        self.angle = 0.
        self.aSpeed = 0.
        self.aAcceleration = 0.

        self.inertia = 1.
        self.mass = 1.

        self.internalForce = np.zeros(2)

        self.world = None

        #This is if i want to apply to looking forward method and compute the next collision of each object
        #self.maxT
        #self.nextPos
        #self.nextSpeed
        #...
        #self.nextAAcceleration

    def setWorld(self, world):
        self.world = world

    def getPos(self):
        return self.pos
    
    def getSpeed(self):
        return self.speed
    
    def getAngle(self):
        return self.angle
    
    def getMass(self):
        return self.mass
    
    def getInertia(self):
        return self.inertia

    def moveTo(self, pos):
        self.pos = pos

    def getCircles(self):
        return []

    def getRects(self):
        return []

    def move(self):
        self.updatePos()

    def isAlive(self):
        return not (self.state == Object.DEAD or (self.pos < self.world.worldMin).any() or (self.pos > self.world.worldMax).any())
    
    def applyForce(self, force): #for now assume these already take into account the mass
        self.acceleration = self.acceleration + force

    def applyPointForce(self, point, force):
        normal = point - self.pos
        self.aAcceleration += np.cross(normal, force)
        self.acceleration = self.acceleration + (force.dot(normal) * normal) / normal.dot(normal)

    def updatePos(self):
        if not self.lockPos:
            self.speed = (self.speed + self.acceleration) * self.world.friction
            self.pos = self.pos + self.world.dt * self.speed * config.FORCEUNIT / self.mass
            self.speed += (self.world.globalForce + self.internalForce) * self.mass * self.world.dt
            self.internalForce.fill(0.)
            self.acceleration.fill(0.)
            self.onFloor = False
        if not self.lockAngle:
            self.aSpeed = (self.aSpeed + self.aAcceleration) * self.world.friction
            self.angle = self.angle + self.world.dt * self.aSpeed * config.FORCEUNIT / self.inertia
            self.aAcceleration = 0.
    
    def action(self, info):
        return

    #Virtual functions
    def getAABB(self):
        return
    
    def collision(self, obj):
        return


class Rect(Object):
    color = np.array([150, 150, 150])

    #Iz = 1/12 width * height * (width * width + height * height)
    
    def __init__(self, diag, lockAngle = True, lockPos = True):
        Object.__init__(self)
        self.diag = diag
        self.inertia = (4 * diag[0] * diag[1] * (diag[0] * diag[0] + diag[1] * diag[1])) / 3
        self.mass = 4 * diag[0] * diag[1]
        self.mass /= 100
        self.inertia /= 100
        self.lockPos = lockPos
        self.lockAngle = lockAngle

    def getDiags(self):
        return utils.rotate([self.diag, np.array([self.diag[0], -self.diag[1]])], self.angle)

    def getRects(self):
        d = self.getDiags()
        vs = (self.pos + d[0], self.pos + d[1], self.pos - d[0], self.pos - d[1])
        return ((vs, self.color),)
    
    def getAABB(self):
        diags = self.getDiags()
        v = np.maximum(np.maximum(np.maximum(diags[0], diags[1]), -diags[0]), -diags[1])
        return (self.pos - v, self.pos + v)

class Lava(Rect):
    color = np.array([255, 0, 0])
    def __init__(self, diag):
        Rect.__init__(self, diag)
    
class Circle(Object):
    color = np.array([0, 0, 0])

    #Iz = 1 / 2 * pi * r ** 4

    def __init__(self, r):
        Object.__init__(self)
        self.radius = r
        r2 = r * r
        self.inertiaInv = 2 / (math.pi * r2 * r2)
        self.mass = math.pi * r2
    
    def getRadius(self):
        return self.radius
    
    def collision(self, obj):
        return
    
    def getCircles(self):
        return ((self.pos, self.radius, self.color),)
    
    def getAABB(self):
        return (self.pos - self.radius, self.pos + self.radius)
    
class Creature(Circle):
    color = np.array([0, 255, 255])
    maxHealth = 100
    def __init__(self, radius = 20):
        Circle.__init__(self, radius)
        self.health = self.maxHealth

    def reward(self, value): #virtual
        return

    def move(self):
        self.activate([])
        self.updatePos()
    
    def modifyHealth(self, dh):
        if self.health + dh <= 0:
            self.health = 0
            self.state = Object.DEAD
            return
        self.health = min(dh + self.health, self.maxHealth)

    def getCircles(self):
        yield (self.pos, self.radius, (255, 0, 0))
        radius = int(self.radius * self.health / self.maxHealth)
        yield (self.pos, radius, self.color)
    
    
class Player(Creature):
    color = np.array([0, 0, 255])
    control = np.array([1.5, 40])

    canJump = False

    def __init__(self):
        Creature.__init__(self)
        self.KEYDOWN = {i:False for i in [273, 274, 275, 276]}
        #self.mass *= 100

    def action(self, info):
        for event in pygame.event.get():
            if event.type in [pygame.KEYDOWN, pygame.KEYUP]:
                self.KEYDOWN[event.key] = (event.type == pygame.KEYDOWN)
            elif event.type == pygame.QUIT:
                self.world.running = False
        self.internalForce[0] = (self.KEYDOWN[275] - self.KEYDOWN[276]) * config.GRAVITY[1] * self.control[0]
        if self.onFloor and self.KEYDOWN[273] and self.canJump:
            self.internalForce[1] = - config.GRAVITY[1] * self.control[1]
            self.canJump = False
        else:
            self.canJump = True
            #self.internalForce[1] = (self.KEYDOWN[274] - self.KEYDOWN[273]) * GRAVITY[1] * self.control[1]

    def collision(self, obj):
        objType = type(obj)
        if objType in [NNetCreature, StupidCreature, AICreature]:
            self.modifyHealth(-1)
        elif objType == Food:
            self.modifyHealth(obj.value)
        elif issubclass(objType, Lava):
            self.modifyHealth(-self.health)

class NNetCreature(Creature):
    def __init__(self, nnet, gemone):
        Creature.__init__(self)
        self.nnet = nnet
        self.genome = genome
    
    def activate(self, info):
        return np.argmax(self.nnet.activate(info))

    def collision(self, obj):
        objType = type(obj)
        if objType in [StupidCreature, Player, AICreature]:
            self.modifyHealth(-1)
            self.push(v)
        else: 
            self.push(v)
    
    def reward(self, value):
        self.genome += value
    
class StupidCreature(Creature):
    def __init__(self, actionProb = 0.05):
        Creature.__init__(self)
        self.actionProb = actionProb
    
    def activate(self, info):
        if random.random() < self.actionProb:
            self.acceleration = 1 - np.random.rand(2) * 2

    def collision(self, obj):
        objType = type(obj)
        if objType in [NNetCreature, Player, AICreature]:
            self.modifyHealth(-10)
            self.push(v)
        else: 
            self.push(v)
    
class AICreature(Creature):
    def __init__(self):
        Creature.__init__(self)

    def activate(self, info):
        return 0

    def collision(self, obj):
        objType = type(obj)
        if objType in [NNetCreature, Player, StupidCreature]:
            self.modifyHealth(-1)
            self.push(v)
        else: 
            self.push(v)

class Food(Circle):
    color = np.array([0, 255, 0])
    def __init__(self, value, radius = 3):
        Circle.__init__(self, radius)
        self.value = value

    def collision(self, obj):
        objType = type(obj)
        if objType == Player:
            self.state = Object.DEAD
