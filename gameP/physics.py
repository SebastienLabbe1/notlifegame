import numpy as np
from objects import *

EPS = 0.000001

def AABBoverlap(aabb1, aabb2):
    return np.max(aabb1[0] - aabb2[1] * aabb1[1] - aabb2[0]) < 0

#elastic collision 2d formula
#v1p = v1 - (2m2 / (m1 + m2)) * (dot(v1 - v2, x1 - x2) / norm2(x1 - x2)) * (x1 - x2) 

def collideCircle2(c1, c2):
    x1 = c1.getPos()
    x2 = c2.getPos()
    x1x2 = x2 - x1
    x1x2N = np.linalg.norm(x1x2)
    if abs(x1x2N) < EPS:
        return True
    overlap = c1.getRadius() + c2.getRadius() - x1x2N
    if overlap < EPS:
        return False
    m1 = c1.getMass()
    m2 = c2.getMass()
    v1 = c1.getSpeed()
    v2 = c2.getSpeed()
    v1v2 = v2 - v1
    a = (2 * v1v2.dot(x1x2) * x1x2) / ((m1 + m2) * x1x2N * x1x2N)
    c1.applyForce(m2 * a)
    c2.applyForce(-m1 * a)
    return True

def collideRect2(rect1, rect2):
    return False

def project(x, v1, v2):
    v = utils.turn90(v2 - v1)
    vN2 = abs(v.dot(v))
    if vN2 < EPS:
        print("bad projection")
        exit()
    return x + ((v1 - x).dot(v) / vN2) * v

def collideCircleRect(circle, rect):
    xc = circle.getPos()
    rc = circle.getRadius()
    xr = rect.getPos()
    d1, d2 = rect.getDiags()
    x1 = xc + d1 - xr
    x2 = xc - d1 - xr
    v1 = d1 - d2
    v2 = d1 + d2
    j = int(v1.dot(x1) > 0) + int(v1.dot(x2) > 0)
    i = int(v2.dot(x1) > 0) + int(v2.dot(x2) > 0)
    if i == 0:
        if j == 0:
            point = xr - d1
        elif j == 1:
            point = project(xc, xr - d1, xr - d2)
        else:
            point = xr + d2
    elif i == 1:
        if j == 0:
            point = project(xc, xr - d1, xr + d2)
        elif j == 1:
            return True
        else:
            point = project(xc, xr + d1, xr - d2)
    elif i == 2:
        if j == 0:
            point = xr - d2
        elif j == 1:
            point = project(xc, xr + d1, xr + d2)
        else:
            point = xr + d1
    x1 = xc
    x2 = point
    x1x2 = x2 - x1
    x1x2N = np.linalg.norm(x1x2)
    if abs(x1x2N) < EPS:
        return True
    overlap = rc - x1x2N
    if overlap < EPS:
        return False
    if x1x2[1] > 0:
        circle.onFloor = True
    m1 = circle.getMass()
    m2 = rect.getMass()
    v1 = circle.getSpeed()
    v2 = rect.getSpeed()
    s1 = circle.stiffness
    s2 = rect.stiffness
    v1v2 = v2 - v1
    p = v1v2.dot(x1x2)
    if p > 0:
        return True
    a = (2 * p * x1x2 * s1 * s2) / (x1x2N * x1x2N)
    if rect.lockPos and rect.lockAngle:
        circle.applyForce(a)
    else:
        circle.applyForce(m2 / (m1 + m2) * a)
        rect.applyPointForce(point, -m1 / (m1 + m2) * a)
    return True

def doPhysics(obj1, obj2):
    if not AABBoverlap(obj1.getAABB(), obj2.getAABB()):
        return False
    if issubclass(type(obj1), Circle):
        if issubclass(type(obj2), Circle):
            return collideCircle2(obj1, obj2)
        elif issubclass(type(obj2), Rect):
            return collideCircleRect(obj1, obj2)
    elif issubclass(type(obj1), Rect):
        if issubclass(type(obj2), Circle):
            return collideCircleRect(obj2, obj1)
        elif issubclass(type(obj2), Rect):
            return collideRect2(obj1, obj2)
