import pygame, numpy, random

class Brain:
	pass

class HumanBrain(Brain):
	ID = 0
	def activate(self, info):
		print(info)
		event = pygame.event.wait()
		while event.type != pygame.KEYDOWN:
			event = pygame.event.wait()
		if event.key == 273:
			print("move")
			return 0
		if event.key == 276:
			print("turn")
			return 1
		if event.key == 275:
			print("turn")
			return 2
		if event.key == 32:
			print("shoot")
			return 3
		if event.key == 274:
			print("eat")
			return 4
		print(event)

class NNetBrain(Brain):
	ID = 1
	def __init__(self, nnet):
		self.nnet = nnet
	
	def activate(self, info):
		return numpy.argmax(self.nnet.activate(info))
	
class DeadBrain(Brain):
	ID = 2
	def __init__(self, values = [1, 3]):
		self.values = values
		self.i = 0
	
	def activate(self, info):
		self.i = (self.i + 1) % len(self.values)
		return self.values[self.i]
	
class PatrolBrain(Brain):
	ID = 3
	def activate(self, info):
		if info[3] > 0.00001:
			return 3
		if random.random() < 0.9:
			return 0
		return random.randint(1, 2)

